package com.bmw.flyway.database;

import com.bmw.common.security.JwtUser;

public class ThreadLocalStorage {
	
	private static final ThreadLocal<String> tenant = new ThreadLocal<>();
	private static final ThreadLocal<String> token = new ThreadLocal<>();
	private static final ThreadLocal<String> gcId = new ThreadLocal<>();
	private static final ThreadLocal<String> ucId = new ThreadLocal<>();
	private static final ThreadLocal<JwtUser> user = new ThreadLocal<>();

	public static void setUser(JwtUser userOb) {
		user.set(userOb);
	}

	public static JwtUser getUser() {
		return user.get();
	}

	public static void setTenantName(String tenantName) {
		tenant.set(tenantName);
	}

	public static String getTenantName() {
		return tenant.get();
	}

	public static void setToken(String inputToken) {
		token.set(inputToken);
	}

	public static String getToken() {
		return token.get();
	}

	public static void removeTenant() {
		tenant.remove();
	}

	public static void setGcId(String inputGcId) {
		gcId.set(inputGcId);
	}

	public static void setUcId(String inputUcId) {
		ucId.set(inputUcId);
	}

	public static String getUcId() {
		return gcId.get();
	}

	public static String getGcId() {
		return ucId.get();
	}

	public static void removeGcId() {
		gcId.remove();
	}

	public static void removeUcId() {
		ucId.remove();
	}
}