package com.bmw.flyway.database;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@ConfigurationProperties(prefix = "collective-datasources")
public class CollectiveDataSourceProperties {

    private Map<String, DataSourceProperties> regionalData;

    public Map<String, DataSourceProperties> getRegionalData() {
        return regionalData;
    }

    public void setRegionalData(Map<String, DataSourceProperties> regionalData) {
        this.regionalData = regionalData;
    }
}
