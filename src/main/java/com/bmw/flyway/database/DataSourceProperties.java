package com.bmw.flyway.database;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.lang.Nullable;

import javax.sql.DataSource;

public class DataSourceProperties {
    private String url;
    private String username;
    private String password;
    private String driverClassName;
    private String sqlPath;
    @Nullable
    private Integer minimumIdle;
    private String schema;

    public String getSqlPath() {
        return sqlPath;
    }

    public void setSqlPath(String sqlPath) {
        this.sqlPath = sqlPath;
    }

    public Integer getMinimumIdle() {
        return minimumIdle;
    }

    public void setMinimumIdle(Integer minimumIdle) {
        this.minimumIdle = minimumIdle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public DataSource createDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(getUrl());
        dataSourceBuilder.username(getUsername());
        dataSourceBuilder.password(getPassword());
        dataSourceBuilder.driverClassName(getDriverClassName());
        DataSource dataSource = dataSourceBuilder.build();
        setMinimumIdleConnections(dataSource);
        return dataSource;
    }

    private void setMinimumIdleConnections(DataSource dataSource) {
        if (dataSource instanceof HikariDataSource) {
            HikariDataSource hikariDataSource = (HikariDataSource) dataSource;
            if (getMinimumIdle() != null) {
                hikariDataSource.setMinimumIdle(getMinimumIdle());
            }
        }
    }

    @Override
    public String toString() {
        return "DataSourceProperties{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", driverClassName='" + driverClassName + '\'' +
                ", sqlPath='" + sqlPath + '\'' +
                ", minimumIdle=" + minimumIdle +
                ", schema='" + schema + '\'' +
                '}';
    }
}
