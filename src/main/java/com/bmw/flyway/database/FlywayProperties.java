package com.bmw.flyway.database;

import lombok.Data;

@Data
public class FlywayProperties {
    private String url;
    private String username;
    private String password;
    private String schemas;
    private String locations;
    private Boolean validateOnMigrate;
}
