package com.bmw.flyway.database;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "collective-flyway")
public class CollectiveFlywayProperties {
    private Map<String, FlywayProperties> regionalData;
}
