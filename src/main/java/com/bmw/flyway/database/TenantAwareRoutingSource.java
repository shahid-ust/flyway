package com.bmw.flyway.database;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class TenantAwareRoutingSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        if(ThreadLocalStorage.getTenantName()!=null){
            return  ThreadLocalStorage.getTenantName();
        }
        else {
            return "def";
        }
    }

}