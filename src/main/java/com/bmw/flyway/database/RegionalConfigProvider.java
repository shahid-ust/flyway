package com.bmw.flyway.database;

import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author UST
 * @version 2.0
 * @since 2019-10-02
 */
@Configuration
@Slf4j
public class RegionalConfigProvider {

    private static final Logger LOG = LoggerFactory.getLogger(RegionalConfigProvider.class);

  //  @Bean(name = "templatePool")
    @Bean
    @Primary
    @ConditionalOnProperty(name = "collective-datasources")
    //public Map<String, JdbcTemplate> createConnectionPool(CollectiveDataSourceProperties collectiveDataSourceProperties) {
    public  DataSource createConnectionPool(CollectiveDataSourceProperties collectiveDataSourceProperties) {
        LOG.info("Entering RegionalConfigProvider->createConnectionPool()");
       // Map<String, JdbcTemplate> templatePool = new HashMap<>();
        Map<Object, Object> databasePool = new HashMap<>();
        Map<String, DataSourceProperties> regionalData = collectiveDataSourceProperties.getRegionalData();
        regionalData.forEach((key, dsProperties) -> {
            LOG.info("RegionalConfigProvider ->createConnectionPool() for the region: {} -c", key);
            DataSource dataSource = dsProperties.createDataSource();
            migrateFlyway(key,dataSource,dsProperties);
            databasePool.put(key, dataSource);
            LOG.info("RegionalConfigProvider->createConnectionPool() - Created DataSource and SqlService for region: {}", key);
        });
        AbstractRoutingDataSource dataSource = new TenantAwareRoutingSource();
        dataSource.setTargetDataSources(databasePool);
        dataSource.afterPropertiesSet();
        LOG.info("Exiting RegionalConfigProvider->createConnectionPool()");
        return dataSource;
    }


    public void migrateFlyway(String tenantName , DataSource dataSource, DataSourceProperties dsProperties) {
        log.info("Start : RegionalConfigProvider : migrateFlyway");
        log.info("tenantName : {}",tenantName);
        if(!tenantName.equalsIgnoreCase("def")){
            String tenantLocation="db/migration/"+tenantName;
            String commonLocation="db/migration/common";
            Flyway flyway = Flyway.configure()
                    .locations(commonLocation,tenantLocation)
                    .baselineOnMigrate(Boolean.TRUE)
                    .dataSource(dataSource)
                    .schemas(dsProperties.getSchema())
                    .load();
            flyway.migrate();
        }
        log.info("End : RegionalConfigProvider : migrateFlyway");
    }

    /*@Bean
    @ConfigurationProperties(prefix="collective-flyway")
    @FlywayDataSource
    public void migrateFlywayDataSource(CollectiveFlywayProperties collectiveFlywayProperties) {
        log.info("Start : RegionalConfigProvider : migrateFlywayDataSource");
        Map<String,FlywayProperties> flywayData=collectiveFlywayProperties.getRegionalData();
        flywayData.forEach((key,flywayDataSource)->{
            log.info("key : {} , schema : {}",key,flywayDataSource.getSchemas());
            Flyway flyway = Flyway.configure()
                    .locations(flywayDataSource.getLocations())
                    .baselineOnMigrate(Boolean.TRUE)
                    .dataSource(flywayDataSource.getUrl(),flywayDataSource.getUsername(),flywayDataSource.getPassword())
                    .schemas(flywayDataSource.getSchemas())
                    .load();
            flyway.migrate();
        });
        log.info("End : RegionalConfigProvider : migrateFlywayDataSource");
    }*/

}


