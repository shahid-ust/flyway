package com.bmw.flyway.security;


import com.bmw.common.security.JwtAuthorizationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * WebSecurityConfig applies authentication by checking the URL pattern
 *
 * @author BMW
 * @version 2.0
 * @since 2019-10-01
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    //@Autowired
    //private JwtTokenUtil jwtTokenUtil;

    //@Autowired
    //private JwtProperties jwtProperties;

    // @Autowired
    //UserService userService;
    @Autowired
    JwtAuthorizationTokenFilter authenticationTokenFilter;

    /** @Autowired
    private RestTemplate restTemplate;

     @LoadBalanced
     @Bean
     public RestTemplate getRestTemplate() {

     return new RestTemplate();
     }
     **/

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // JwtAuthorizationTokenFilter authenticationTokenFilter =
        //  new JwtAuthorizationTokenFilter( jwtProperties);
        //authenticationTokenFilter.setRestTemplate(restTemplate);

        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/general/**").permitAll()
                .antMatchers("/secured/**").authenticated()
                .and()
                .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


}