package com.bmw.flyway.security;

import com.bmw.common.security.JwtUser;
import com.bmw.flyway.database.ThreadLocalStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SetThreadStorageFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(SetThreadStorageFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			log.debug("inside SetThreadStorageFilter: doFilter()");
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			String region = req.getHeader("X-REGION-ID");
			ThreadLocalStorage.setTenantName(region);
			ThreadLocalStorage.setToken(req.getHeader("Authorization"));
			JwtUser user = (JwtUser) request.getAttribute("currentUser");
			ThreadLocalStorage.setUser(user);
			res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
			res.setHeader("Access-Control-Max-Age", "3600");
			res.setHeader("Access-Control-Allow-Headers",
					"header,x-requested-with, authorization, x-auth-token, origin,content-type,accept,Cache-Control,Pragma,Expires,X-REGION-ID,X-ENV-ID,x-region-id,x-env-id");
			res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
			res.setHeader("Pragma", "no-cache"); // HTTP 1.0
			res.setDateHeader("Expires", 0); // Proxies.
			if ("OPTIONS".equalsIgnoreCase(req.getMethod())) {
				res.setStatus(HttpServletResponse.SC_OK);
			} else {
				chain.doFilter(req, res);
			}

		} finally {
			ThreadLocalStorage.removeTenant();
			ThreadLocalStorage.removeGcId();
			ThreadLocalStorage.removeUcId();
			ThreadLocalStorage.setTenantName(null);
			ThreadLocalStorage.setGcId(null);
			ThreadLocalStorage.setUcId(null);
		}
	}

	@Override
	public void destroy() {
		ThreadLocalStorage.removeTenant();
		ThreadLocalStorage.setTenantName(null);
	}

}