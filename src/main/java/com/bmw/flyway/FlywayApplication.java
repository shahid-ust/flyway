package com.bmw.flyway;

import com.bmw.common.security.JwtProperties;
import com.bmw.flyway.database.CollectiveDataSourceProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@EnableConfigurationProperties({JwtProperties.class, CollectiveDataSourceProperties.class})
@SpringBootApplication(scanBasePackages = "com.bmw.*")
@PropertySource("classpath:application-${spring.profiles.active}.yml")
public class FlywayApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlywayApplication.class, args);
	}

}
