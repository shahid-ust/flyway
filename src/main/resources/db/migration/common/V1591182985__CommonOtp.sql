DROP TABLE smoss.otp;

CREATE TABLE IF NOT EXISTS smoss.otp
(
    id bigint NOT NULL,
    target_id character varying ,
    otp character varying ,
    event_type character varying ,
    send_count bigint,
    CONSTRAINT pk_otp PRIMARY KEY (id)
);

INSERT INTO smoss.otp VALUES(1,'test-target','xxxx','mobileEvent',0);
