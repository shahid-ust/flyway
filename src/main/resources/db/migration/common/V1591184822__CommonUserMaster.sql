CREATE TABLE IF NOT EXISTS smoss.user_master
(
    id bigint NOT NULL,
    name character varying ,
    email character varying ,
    mobile character varying ,
    address character varying ,
    CONSTRAINT pk_user_master PRIMARY KEY (id)
);

CREATE SEQUENCE smoss.user_seq
INCREMENT 1
MINVALUE 1
START 1;
